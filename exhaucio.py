# Importamos Librerias
import math

# Declaración de variables Iniciales
n = 6
ladoInicial = 1

# Creando un ciclo for
for i in range(1, 20): 
  x = ladoInicial/2
  a = math.sqrt(1-(x**2)) 
  b = 1 - a
  nuevoLado = math.sqrt(x**2+b**2) 
  P = n*ladoInicial
  D = P/2
  print(n, x, a, b, nuevoLado, P, D)
  
  ladoInicial = nuevoLado
  n = n * 2
